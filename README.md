Rebuilt with Tkinter - simplified quite a bit.

This will take the input from a card reader such as https://www.rfideas.com/products/readers/dual-frequency/wave-id-plus and record it to a csv locally from where more complicated things can be done.

This program will present a full screen with the Materials Department Logo, a text entry box and a down arrow alongside an instruction to swipe your card to record attendance. 

It can be run automatically on start up, for example on a raspberry pi set up to log in automatically. The shift from X to wayland has complicated things (https://github.com/thagrol/Guides/blob/main/bookworm.pdf) but adding the appropriate details to /home/[user]/.config/autostart/swipe_in.desktop should work and without elevated privileges. The exsiting example desktop file can be used and will probably work:


`mkdir $HOME/.config/autostart/` 
then:

`cp $HOME/lab_kiosk/example.desktop $HOME/.config/autostart/swipe_in.desktop`


This does assume that the script exists in `~/lab_kiosk` which it will be if you just clone it from the home directory. It should be executable.

Worth noting that the screen orientation for the official touchscreen for the RPi is upside down if you use the official case - this can be configured in the RPi configuration either via the command line or GUI. 

Included is a CLI version - which could be used where a display isn't needed or for entirely headless systems. It's probably easiest to set that up as `python3 ~/lab_kiosk/log_in.py` in .bashrc and again worth checking the orientation of the screen, see e.g. https://askubuntu.com/questions/237963/how-do-i-rotate-my-display-when-not-using-an-x-server, the commands for this can be added to .bashrc as well.
