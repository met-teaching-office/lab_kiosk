#!/usr/bin/env python3

from tkinter import *
from datetime import datetime, time
from time import sleep
import os
from images import MSM_logo_image, down_arrow_image, green_tick_image, positive, negative, late_image

# seem to lose the focus if we start too quick...
sleep(5)

home_directory = os.path.expanduser( '~' )
log_file_name = os.path.join(home_directory, 'attendance_log.csv')

location = 'IB-lab' # physical location will be recorded in the csv - edit here if you have more than one set up
#location = 'IB-lab'
#location = 'lecture-room'

def is_time_between(begin_time, end_time, check_time=None):
    # If check time is not given, default to current UTC time
    check_time = check_time or datetime.now().time()
    if begin_time < end_time:
        return check_time >= begin_time and check_time <= end_time
    else: # crosses midnight
        return check_time >= begin_time or check_time <= end_time

if location == 'IA-lab':
    start_late_A = time(13,45)
    end_late_A = time(15,15)
    start_late_B = time(15,30)
    end_late_B = time(17,00)
elif location == 'IB-lab':
    start_late_A = time(9,15)
    end_late_A = time(11,30)
    start_late_B = time(14,00)
    end_late_B = time(16,15)
else:
    start_late_A = time(0,1)
    end_late_A = time(0,2)
    start_late_B = time(0,3)
    end_late_B = time(0,4)



ws = Tk()
ws.title('Swipe In')

#making this full screen doesn't work right away?! Add this and it does?!
#ws.attributes('-fullscreen', True)
ws.after(500, lambda: ws.attributes('-fullscreen', True))

#this variable will be dynamically linked to the TK text entry box
identity = StringVar(ws,value='')
message = StringVar(ws, value='Swipe your card to record your attendance')


def show_late():
    message.set('Recorded as late!')
    down_arrow.config(image=late_image)


def show_recorded():
    message.set('Recorded!')
    down_arrow.config(image=positive_face)

def show_int_error():
    down_arrow.config(image=negative_face)
    message.set('Something went wrong - id not a number')

def show_length_error():
    down_arrow.config(image=negative_face)
    message.set('Something went wrong - id not expected size')

def reset_view(t=500):
    entry.delete(0,'end')
    down_arrow.after(t, lambda :down_arrow.config(image=arrow_img))
    middle_text.after(t, lambda :message.set('Swipe your card to record your attendance'))

#this is the function we're binding to the return key being entered in the text box -  the mifare card reader retunrs the mifare id and the return key when swiped
def mifare_read(x):
    mifare = identity.get()
    if mifare == 'quit':
        ws.destroy()
        exit()
    try:
        val = int(mifare)
        #this allows us to end the program (though alt + F4 would do it too...
    except ValueError:
        show_int_error()
    else:
        if (len(mifare) > 7) and (len(mifare) < 13):
            with open(log_file_name,'at') as f:
                f.write(mifare + ',' + datetime.ctime(datetime.now()) + ',' + location + '\n')
            # change the down arrow to a tick then back again
            if location == 'IB-lab':
                if is_time_between(start_late_A, end_late_A) or is_time_between(start_late_B, end_late_B):
                    show_late()
                else:
                    show_recorded()
            else:
                show_recorded()
        else: #the length is wrong - don't record...
            show_length_error()
    finally:
        #number is ms delay before resetting to original view
        reset_view(700)


frame = Frame(ws, bg='white')
frame.place(relwidth=1, relheight=1)
middle_text = Label(frame,
                    textvariable=message,
                    font=('Times', 20),
                    bg='white'
                    )

green_tick = PhotoImage(data=green_tick_image)
positive_face = PhotoImage(data=positive)
negative_face = PhotoImage(data=negative)
late_image = PhotoImage(data=late_image)
logo_img = PhotoImage(data=MSM_logo_image)
logo = Label(frame, image=logo_img, bg='white')

arrow_img = PhotoImage(data=down_arrow_image)
down_arrow = Label(frame, image=arrow_img, bg='white')

entry = Entry(frame, width=12, textvariable=identity)#, show="*")

logo.pack(pady=20)
middle_text.pack(pady=20)
down_arrow.pack(pady=20)
entry.pack(pady=20)

entry.focus()
entry.bind("<Return>", mifare_read)
ws.config(cursor="none")
ws.mainloop()
