#!/usr/bin/env python3

import time
import os

home_directory = os.path.expanduser( '~' )
log_file_name = os.path.join(home_directory, 'attendance_log.csv')

try:
    from local_details import *
except:
    pass

if not 'location' in globals():
    location = 'lab'  # add location to local details to customise

n = 0


for n in range(30):
    print('')

start = time.time()
while True:
    identity = input("Swipe your card:")
    print('Attendance recorded: ' + time.ctime())
    time.sleep(0.25)
    print('')
    print('')
    
    with open(log_file_name,'at') as f:
        f.write(identity + ',' + time.ctime() + ',' + location + '\n')

